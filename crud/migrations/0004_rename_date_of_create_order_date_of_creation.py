# Generated by Django 4.2.3 on 2023-07-31 10:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('crud', '0003_alter_order_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='date_of_create',
            new_name='date_of_creation',
        ),
    ]
