from django.urls import path,include
from . import views
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView, TokenVerifyView
app_name='crud'
urlpatterns = [
     path('',views.ApiOverview,name='home'),
     path('all/', views.view_orders, name='view_orders'),
     path('view-order/',views.view_order,name='view_order'),
     path('add-order/',views.add_order,name='add_order'),
      path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('token/verify/', TokenVerifyView.as_view(), name='token_verify'),
]