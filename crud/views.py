from rest_framework.decorators import api_view,permission_classes,authentication_classes
from rest_framework.response import Response
from .models import Order
from .serializers import OrderSerializer
from rest_framework import serializers
from rest_framework import status
from rest_framework.permissions import IsAuthenticated,AllowAny




@api_view(['GET'])
@permission_classes([AllowAny])
def ApiOverview(request):
	api_urls = {
		'All_items': '/all',
		'Change/view/delete order': 'view-order/?id=order_id',
		'Add':'add-order/',
        'GetAuthToken':'token/',
        'RefreshAuthToken':'token/refresh/',
        'VerifyAuthToken':'token/verify/'
	}

	return Response(api_urls)


@api_view(['GET'])
def view_orders(request):
    if request.query_params.get('status'):
         orders=Order.objects.filter(status=request.query_params.get('status'))
    else:
        orders = Order.objects.all()  
    serializer = OrderSerializer(orders,many=True)
    return Response(serializer.data)
   

@api_view(['POST']) 
def add_order(request):
    order = OrderSerializer(data=request.data)
    if Order.objects.filter(**request.data).exists():
        raise serializers.ValidationError('This data already exists')
 
    if order.is_valid():
        order.save()
        return Response(order.data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)


@api_view(['PUT','DELETE','GET'])
def view_order(request):
    if request.query_params.get('id'):
        try:
            order=Order.objects.get(pk=request.query_params.get('id'))
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
        if request.method=='PUT':
            serializer=OrderSerializer(order,data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        elif request.method=='GET':
            serializer=OrderSerializer(order)
            return Response(serializer.data)
        elif request.method=='DELETE':
            order.delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)
    

