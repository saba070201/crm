from django.db import models
from django.contrib.auth.models import User

class Order(models.Model):
    ACCEPTED='принято'
    AT_WORK='в работе'
    DONE='выполнено'
    DONE_AND_PAID='выполнено и оплачено'
    STATUS_CHOICES = [
        (ACCEPTED, 'принято'),
        (AT_WORK, 'в работе'),
        (DONE, 'выполнено'),
        (DONE_AND_PAID, 'выполнено и оплачено'),
       
    ]
    title=models.CharField(max_length=200,blank=True,null=False)
    memo=models.TextField(blank=True,null=False)
    date_of_creation=models.DateTimeField(auto_now=True)
    price=models.IntegerField(blank=True,null=True)
    user=models.ForeignKey(User,on_delete=models.CASCADE,default=1)
    status=models.CharField(max_length=50,choices=STATUS_CHOICES,default=ACCEPTED)
    def __str__(self) -> str:
        return str('заказ:')+str(self.id)
    

