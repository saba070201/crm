# CRM 
## Getting started
### Build 
```bash
docker-compose up 
docker-compose run django python manage.py makemigrations 
docker-compose run django python manage.py migrate
docker-compose run django python manage.py createsuperuser
```
### Api urls 
Go to path http://127.0.0.1:8000/api
### Auth 
Send a POST request with registered user data to the address http://127.0.0.1:8000/api/token/.
Get the token in your response by the key "access".
Send any request to a different URL specified by the link, http://127.0.0.1:8000/api/. In the Headers, specify: "Authorization": "Bearer access_token"


